/**
 * Optimize MP4 Video Uploads for web
 *
 * Logs at https://console.cloud.google.com/logs/viewer?project=videooptimizer-216820&authuser=4&minLogLevel=0&expandAll=false&timestamp=2018-09-18T21:08:04.097000000Z&customFacets=&limitCustomFacetWidth=true&interval=NO_LIMIT&resource=gae_app&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fstdout&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fstderr&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fappengine.googleapis.com%252Frequest_log&scrollTimestamp=2018-09-18T21:08:59.364904000Z
 *
 * Refs:
 * 0. https://github.com/firebase/functions-samples/blob/master/ffmpeg-convert-audio/functions/index.js
 * 1. https://stackoverflow.com/questions/42773497/can-you-call-out-to-ffmpeg-in-a-firebase-cloud-function
 * 2. https://stackoverflow.com/questions/44469537/cloud-functions-for-firebase-completing-long-processes-without-touching-maximum/44472980#44472980
 * 3. https://cloud.google.com/functions/docs/concepts/exec
 * 4. https://cloud.google.com/functions/docs/concepts/nodejs-8-runtime
 * 5. https://github.com/Scew5145/GCSConvertDemo
 */

const express = require('express');
const timeout = require('connect-timeout');
const request_promise = require('request-promise');
const Promise = require('bluebird');
const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');
const version = "0.0.16";
const DEBUG=false;

/** Helpers **/


function promisifyCommand(command) {
  return new Promise((resolve, reject) => {
    command
      .on('end', () => {
        resolve();
      })
      .on('error', (error) => {
        reject(error);
      })
      .run();
  });
}

const ffmpegLogger = {
  debug: function(arg) { if (DEBUG) console.log('[DEBUG] ' + arg); },
  info: function(arg) { if (DEBUG) console.log('[INFO] ' + arg); },
  warn: function(arg) { if (DEBUG) console.log('[WARN] ' + arg); },
  error: function(arg) { console.log('[ERROR] ' + arg); }
};

async function Encode(object) {
  const bucket = gcs.bucket(object.bucket); // The Storage bucket that contains the file.
  const srcBucketPath = object.name; // File path in the bucket.

  const fileName = path.basename(srcBucketPath);
  const srcLocalPath = path.join(os.tmpdir(), fileName);
  const outLocalPath = path.join(os.tmpdir(), fileName + '.mp4');
  const outBucketPath = path.join(path.dirname(srcBucketPath), fileName + '.mp4');

  console.log("Encoding " + bucket + srcBucketPath);

  if (fs.existsSync(srcLocalPath)) {
    console.log('Skipping: File already downloaded, so assume this is a dup process');
    return null;
  }

  console.log('Downloading ' + srcBucketPath + ' to', srcLocalPath);
  await bucket.file(srcBucketPath).download({destination: srcLocalPath});

  console.log('Creating ' + outLocalPath);
  const ffmpegCmdMp4 = ffmpeg({ source: srcLocalPath, logger: ffmpegLogger, timeout: 1200 })
    .setFfmpegPath(ffmpeg_static.path)

    .on('progress', function(progress) {
      console.log('Processing: ' + progress.percent + '% done'); // not always available
      console.dir(progress);
    })
    // .on('stderr', function(stderrLine) {
    //   console.log('Stderr output: ' + stderrLine);
    // })

    // Audio Settings: https://trac.ffmpeg.org/wiki/Encode/MP3
    .audioCodec('libmp3lame')
    .audioChannels(1)
    // .audioBitrate(64)
    .audioQuality(7)

    // Mp4 settings
    .videoCodec('libx264')
    .videoBitrate(1024)
    .size('1920x?')
    .format('mp4')
    .output(outLocalPath);
  await promisifyCommand(ffmpegCmdMp4);

  console.log('Uploading '+ outLocalPath + ' to ' + outBucketPath);
  await bucket.upload(outLocalPath, {destination: outBucketPath});

  console.log('Deleting tmp files');
  fs.unlinkSync(srcLocalPath);
  fs.unlinkSync(outLocalPath);

  return Promise.resolve();
}


/** Express App **/

const app = express();
app.use(express.json());
app.use(timeout('21m'));

app.get('/', async (req, res) => {
  res.status(200).send("VideoOptimizer is listening for GSC storage objects to encode");
});

app.post('/async', async (req, res) => {
// export const VideoUploaded = functions.runWith(runtimeOpts).storage.object().onFinalize(async (object) => {
  if (!req.body.object) return res.status(500).send('object is missing.');
  if (!req.body.object.bucket) return res.status(500).send('object.bucket is missing.');
  if (!req.body.object.name) return res.status(500).send('object.name is missing.');
  if (!req.body.object.contentType) return res.status(500).send('object.contentType is missing.');
  if (!req.body.object.contentType.startsWith('video/')) return res.status(500).send('object.contentType is not a video.');

  // Intentionally not waiting for this request to complete before sending 200 back
  // request_promise({
  //   url: 'https://videooptimizer-216820.appspot.com/sync',
  //   method: 'POST',
  //   body: {
  //     object: req.body.object,
  //   },
  //   json: true
  // }).then(function(res) {
  //   console.log('Sync Encoding Request Response: ' + res);
  // }).catch(function(error) {
  //   console.log(error.message);
  // });

  Encode(req.body.object);

  res.status(200).send('Request Received');

});

app.post('/sync', async (req, res) => {
// export const VideoUploaded = functions.runWith(runtimeOpts).storage.object().onFinalize(async (object) => {
  if (!req.body.object) return res.status(500).send('object is missing.');
  if (!req.body.object.bucket) return res.status(500).send('object.bucket is missing.');
  if (!req.body.object.name) return res.status(500).send('object.name is missing.');
  if (!req.body.object.contentType) return res.status(500).send('object.contentType is missing.');
  if (!req.body.object.contentType.startsWith('video/')) return res.status(500).send('object.contentType is not a video.');

  try {
    await Encode(req.body.object);
    res.status(200).send('Encode complete');
  } catch (e) {
    console.error(e);
    res.status(500).send('Uncaught error: ' + e.message);
  }

});


if (module === require.main) {
  const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port} with version ${version}`);
    console.log(server.address());
  });
}

module.exports = app;

// Encode({
//   bucket: "gs://get-candeo.appspot.com",
//   "name": "video_files/wbbpyjgTudQY9QNLCAl7",
//   "contentType": "video/mp4"
// });

# 1 - Hello world

![Build Status][ci-badge]

This folder contains the sample code for a small App Engine application that
displays a short message. See the instructions below for how to configure, run,
and deploy this sample.

This project is abandoned, because I couldn't get it working on any autoscaling. When auto or basic scaling is enabled, AppEngine terminates long running processes.

[ci-badge]: https://storage.googleapis.com/nodejs-getting-started-tests-badges/1-tests.svg

# Simple instructions

1.  Install [Node.js](https://nodejs.org/en/).

    * Optional: Install [Yarn](https://yarnpkg.com/).

1.  Install [git](https://git-scm.com/).
1.  Create a [Google Cloud Platform project](https://console.cloud.google.com).
1.  Install the [Google Cloud SDK](https://cloud.google.com/sdk/).

    * After downloading the SDK, initialize it:

            gcloud init

1.  Clone the repository:

        git clone https://github.com/GoogleCloudPlatform/nodejs-getting-started.git

1.  Change directory:

        cd nodejs-getting-started/1-hello-world

1.  Install dependencies using NPM or Yarn:

    * Using NPM:

            npm install

    * Using Yarn:

            yarn install

1.  Start the app using NPM or Yarn:

    * Using NPM:

            npm start

    * Using Yarn:

            yarn start

1.  View the app at [http://localhost:8080](http://localhost:8080).

1.  Stop the app by pressing `Ctrl+C`.

1.  Deploy the app:

        gcloud app deploy

1.  View the deployed app at https://videooptimizer-216820.appspot.com

1.  View logs: https://console.cloud.google.com/logs/viewer?project=videooptimizer-216820&authuser=4&minLogLevel=0&expandAll=false&timestamp=2018-09-18T21:08:04.097000000Z&customFacets=&limitCustomFacetWidth=true&interval=NO_LIMIT&resource=gae_app&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fstdout&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fstderr&logName=projects%2Fvideooptimizer-216820%2Flogs%2Fappengine.googleapis.com%252Frequest_log&scrollTimestamp=2018-09-18T21:08:59.364904000Z
